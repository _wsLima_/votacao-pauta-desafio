package br.com.wslima.api.persistence.repositories;

import br.com.wslima.api.persistence.repositories.models.Pauta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PautaRepository extends JpaRepository<Pauta, Long> {
}
